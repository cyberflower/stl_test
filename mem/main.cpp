#include <iostream>
#include <stdint.h>
#include <vector>
#include <list>
#include <set>
#include <map>
#include <unordered_set>
#include "gmem.h"
using namespace std;
struct Obj{
	uint64_t sth;
};
void vector_test(){
	vector<Obj> v;
	v.reserve(5);
	v.push_back(Obj());
	v.push_back(Obj());
	v.push_back(Obj());
	v.push_back(Obj());
	v.push_back(Obj());
	v.push_back(Obj());
	v.push_back(Obj());
	v.push_back(Obj());
	v.push_back(Obj());
}
void list_test(){
	list<uint64_t> l;
	l.push_back(1);
	l.push_back(2);
	l.push_back(3);
	l.push_back(5);
	l.push_back(4);
	for(uint64_t x:l) cout<<x<<endl;
}
void set_test(){
	set<uint64_t> s;
	s.insert(1);
	s.insert(2);
	s.insert(3);
	s.insert(2);
	s.insert(0);
	for(uint64_t x:s) cout<<x<<endl;
}
void map_test(){
	map<uint64_t, uint64_t> m;
	m[1]=100;
	m[3]=50;
	m[2]=75;
	for(auto x:m) cout<<x.first<<" "<<x.second<<endl;
}
void unordered_set_test(){
	unordered_set<uint64_t> s;
	s.insert(1);
	s.insert(3);
	s.insert(4);
	for(auto x:s) cout<<x<<endl;
}
int main(void){
	gmem_set_verbose(true);
	//vector_test();
	//list_test();
	//set_test();
	//map_test();
	unordered_set_test();
	return 0;
}
